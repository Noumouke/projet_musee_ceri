package com.example.musee_ceri;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static Musee_Dbhelper dbhelper;
    RecyclerView recyclerView;
    Musee_Adapter adapter;
    public static SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);


        dbhelper=new Musee_Dbhelper(this);
        dbhelper.deleteAllMusses();
        // this.dbhelper.populate();
        this.recyclerView = findViewById(R.id.RecyclerView);
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();

        populateView();
    }

    public void populateView(){
        adapter = new Musee_Adapter(this, this.dbhelper.getAllMusses());
        for(Musee item : this.dbhelper.getAllMusses()){
            //Log.d("display_item (M1)",item.toString());
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemViewCacheSize(0);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
    }

    public class MyAsyncTask extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONResponseHandlerMusee jsonHandler = new JSONResponseHandlerMusee(null);
            Musee item = new Musee();
            try {
                dbhelper.deleteAllMusses();
                List<String> ids = jsonHandler.readIdsJsonStream(WebServiceUrl.buildSearchIds().openStream());
                for(String id : ids){

                    item.setIdwb(id);
                    JSONResponseHandlerMusee jh = new JSONResponseHandlerMusee(item);
                    jh.readJsonStream(WebServiceUrl.buildSearchItem(id).openStream());
                    Log.d("display_item: (MA) ",jh.getItem().toString());
                    item = jh.getItem();


                    dbhelper.addMusses(item);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            populateView();

        }


    }

    @Override
    public void onRefresh() {
        for (Musee musee_item : MainActivity.dbhelper.getAllMusses()) {
            MyAsyncTask myAsyncTask = new MyAsyncTask();
            myAsyncTask.execute();

        }
    }

}
